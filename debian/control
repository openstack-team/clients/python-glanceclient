Source: python-glanceclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Corey Bryant <corey.bryant@canonical.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-ddt,
 python3-fixtures,
 python3-hacking,
 python3-keystoneauth1,
 python3-openssl,
 python3-openstackdocstheme,
 python3-os-client-config,
 python3-oslo.i18n,
 python3-oslo.utils,
 python3-prettytable,
 python3-requests,
 python3-requests-mock,
 python3-sphinxcontrib.apidoc,
 python3-stestr,
 python3-subunit,
 python3-tempest,
 python3-testscenarios,
 python3-testtools,
 python3-warlock,
 python3-wrapt,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-glanceclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-glanceclient.git
Homepage: http://glance.openstack.org

Package: python-glanceclient-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Client library for Openstack glance server - doc
 The Glance project provides services for discovering, registering, and
 retrieving virtual machine images over the cloud. They may be stand-alone
 services, or may be used to deliver images from object stores, such as
 OpenStack's Swift service, to Nova's compute nodes.
 .
 This is a client for the Glance which uses the OpenStack Image API. There's a
 Python API (the "glanceclient" module), and a command-line script ("glance").
 .
 This package provides the documentation and man page.

Package: python3-glanceclient
Architecture: all
Depends:
 python3-keystoneauth1,
 python3-openssl,
 python3-oslo.i18n,
 python3-oslo.utils,
 python3-pbr,
 python3-prettytable,
 python3-requests,
 python3-warlock,
 python3-wrapt,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-glanceclient-doc,
Description: Client library for Openstack glance server - Python 3.x
 The Glance project provides services for discovering, registering, and
 retrieving virtual machine images over the cloud. They may be stand-alone
 services, or may be used to deliver images from object stores, such as
 OpenStack's Swift service, to Nova's compute nodes.
 .
 This is a client for the Glance which uses the OpenStack Image API. There's a
 Python API (the "glanceclient" module), and a command-line script ("glance").
 .
 This package provides the Python 3.x module.
